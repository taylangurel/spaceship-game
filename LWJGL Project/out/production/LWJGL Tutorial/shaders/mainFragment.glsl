//FRAGMENT SHADER
#version 330 core

in vec3 passColor;
in vec2 passTextureCoord;
in vec3 passLightPosition;
in vec3 passLightColor;
in vec3 passNormal;
in vec3 passFragmentPosition;

out vec4 outColor;

uniform sampler2D tex;
uniform vec3 lightColor;

void main() {
    vec3 normal = normalize(passNormal);
    vec3 lightPosition = normalize(passLightPosition - passFragmentPosition);
    vec3 diffuse = max(dot(normal,lightPosition),0.0) * passLightColor;

    float ambientStrength = 0.5;
    vec3 ambient = ambientStrength * passLightColor;


    vec4 textureColor = texture(tex, passTextureCoord); //For adding textures
	//outColor = vec4(passColor, 1.0); //For coloring

    outColor = vec4(ambient+diffuse,1.0)*textureColor;

    //outColor = vec4(color,1.0);
}