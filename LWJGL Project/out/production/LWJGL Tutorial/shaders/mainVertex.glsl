//VERTEX SHADER
#version 330 core

in vec3 position;
in vec3 color;
in vec2 textureCoord;
in vec3 normal;

out vec3 passColor;
out vec2 passTextureCoord;
out vec3 passNormal;
out vec3 passLightPosition;
out vec3 passLightColor;
out vec3 passFragmentPosition;

uniform mat4 model; //mat4 -> matrix in our shader language, model-> transformation matrix
uniform mat4 view;
uniform mat4 projection;
uniform vec3 lightPosition;
uniform vec3 lightColor;

void main() {
	gl_Position = projection * view * model * vec4(position, 1.0) ; //the order should be M V P or P V M
	passColor = color;
	passTextureCoord = textureCoord;
	passNormal = normal;
	passFragmentPosition = vec3(model*vec4(position, 1.0));
	passLightColor= lightColor;
	passLightPosition = lightPosition;
}