package main;

import engine.graphics.*;
import engine.hud.FontType;
import engine.hud.GUIText;
import engine.hud.TextManager;
import engine.io.Input;
import engine.io.ModelLoader;
import engine.io.Window;
import engine.maths.Vector2f;
import engine.maths.Vector3f;
import engine.objects.Camera;
import engine.objects.GameObject;
import org.lwjgl.glfw.GLFW;
import engine.graphics.Light;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Main implements Runnable {
    public Thread game;
    public Window window;
    public Renderer renderer;
    public Shader shader;
    private TextManager textManager;
    private FontType font;
    private GUIText timeText, healthText, gameOverText,fuelText;
    private final long startTime = Calendar.getInstance().getTimeInMillis();
    private int healthPoint = 100;
    private int fuel = 100;
    private long lastFuelTime=0;



    public final int FPS = 60;

    public final static int WIDTH = 1000, HEIGHT = 600;

    public Mesh meshCube = new Mesh(new Vertex[] {
            //Back face
            new Vertex(new Vector3f(-1f,  1f, -1f),new Vector3f(-1f,  1f, -1f), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(-1f, -1f, -1f),new Vector3f(-1f, -1f, -1f), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f( 1f, -1f, -1f),new Vector3f( 1f, -1f, -1f), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f( 1f,  1f, -1f), new Vector3f( 1f,  1f, -1f),new Vector2f(1.0f, 0.0f)),

            //Front face
            new Vertex(new Vector3f(-1f,  1f,  1f),new Vector3f(-1f,  1f,  1f), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(-1f, -1f,  1f),new Vector3f(-1f, -1f,  1f),new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f( 1f, -1f,  1f),new Vector3f( 1f, -1f,  1f), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f( 1f,  1f,  1f),new Vector3f( 1f,  1f,  1f), new Vector2f(1.0f, 0.0f)),

            //Right face
            new Vertex(new Vector3f( 1f,  1f, -1f), new Vector3f( 1f,  1f, -1f),new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f( 1f, -1f, -1f),new Vector3f( 1f, -1f, -1f), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f( 1f, -1f,  1f),new Vector3f( 1f, -1f,  1f), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f( 1f,  1f,  1f),new Vector3f( 1f,  1f,  1f), new Vector2f(1.0f, 0.0f)),

            //Left face
            new Vertex(new Vector3f(-1f,  1f, -1f),new Vector3f(-1f,  1f, -1f), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(-1f, -1f, -1f),new Vector3f(-1f, -1f, -1f), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f(-1f, -1f,  1f),new Vector3f(-1f, -1f,  1f), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f(-1f,  1f,  1f),new Vector3f(-1f,  1f,  1f), new Vector2f(1.0f, 0.0f)),

            //Top face
            new Vertex(new Vector3f(-1f,  1f,  1f),new Vector3f(-1f,  1f,  1f), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(-1f,  1f, -1f),new Vector3f(-1f,  1f, -1f), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f( 1f,  1f, -1f),new Vector3f( 1f,  1f, -1f), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f( 1f,  1f,  1f), new Vector3f( 1f,  1f,  1f),new Vector2f(1.0f, 0.0f)),

            //Bottom face
            new Vertex(new Vector3f(-1f, -1f,  1f),new Vector3f(-1f, -1f,  1f), new Vector2f(0.0f, 0.0f)),
            new Vertex(new Vector3f(-1f, -1f, -1f),new Vector3f(-1f, -1f, -1f), new Vector2f(0.0f, 1.0f)),
            new Vertex(new Vector3f( 1f, -1f, -1f),new Vector3f( 1f, -1f, -1f), new Vector2f(1.0f, 1.0f)),
            new Vertex(new Vector3f( 1f, -1f,  1f),new Vector3f( 1f, -1f,  1f), new Vector2f(1.0f, 0.0f)),
    }, new int[] {
            //Back face
            0, 1, 3,
            3, 1, 2,

            //Front face
            4, 5, 7,
            7, 5, 6,

            //Right face
            8, 9, 11,
            11, 9, 10,

            //Left face
            12, 13, 15,
            15, 13, 14,

            //Top face
            16, 17, 19,
            19, 17, 18,

            //Bottom face
            20, 21, 23,
            23, 21, 22
    }, new Material("/textures/rock-texture-square.png"));

public Mesh meshRoad = new Mesh(new Vertex[]{
            new Vertex(new Vector3f(-50f,  0.5f, 1000f), new Vector3f(0f, -1.0f, 0.0f), new Vector2f(0.0f, 0.0f)), // 0, top left, vector2f is counter clockwise
            new Vertex(new Vector3f(-50f, -0.5f, -100f), new Vector3f(0f, -1.0f, 0.0f), new Vector2f(0.0f, 1.0f)), // 1, bottom left
            new Vertex(new Vector3f( 50f, -0.5f, -100f), new Vector3f(0.0f, -1.0f, 0f), new Vector2f(1.0f, 1.0f)), // 2, bottom right
            new Vertex(new Vector3f( 50f,  0.5f, 1000f),new Vector3f(0f, -1.0f, 0.0f), new Vector2f(1.0f, 0.0f))  // 3, top right
}, new int[]{
            0, 1, 2, //indicating the order of the drawing
           0, 2, 3
    }, new Material("/textures/2k_mars.png"));

    public List<GameObject> objects = new ArrayList<>();

    public GameObject cube = new GameObject(new Vector3f(0,0,0),new Vector3f(0,0,0), new Vector3f(1,1,1),meshCube, GameObject.Type.CUBE);
    public GameObject road = new GameObject(new Vector3f(0,-5,0),new Vector3f(0,0,0), new Vector3f(1,1,1),meshRoad, GameObject.Type.ROAD);


    public Mesh mesh = ModelLoader.loadModel("resources/models/spaceship.obj", "/textures/openstreetmap.png");

    public Light light = new Light(new Vector3f(10f,10f,10f), new Vector3f(0.95f,0.95f,0.48f));

    public GameObject spaceship = new GameObject(new Vector3f(0, 0, 0),new Vector3f(0, 0, 0),new Vector3f(1, 1, 1), mesh, GameObject.Type.SPACESHIP); // scale should be 1


    public Camera camera = new Camera(new Vector3f(0,0,1), new Vector3f(0,180,0));

    public Mesh fuelMesh = ModelLoader.loadModel("resources/models/jerrycan-scale5.obj", "/textures/red.png");
    public GameObject fuelTank = new GameObject(new Vector3f(0,0,0),new Vector3f(0,0,0), new Vector3f(1,1,1),fuelMesh, GameObject.Type.FUEL);


    public void start(){
        game = new Thread(this, "game");
        game.start();
    }

    public void init(){
        System.out.println("Initializing the game!");
        window = new Window(WIDTH, HEIGHT, "Spaceship Game");
        shader = new Shader("/shaders/mainVertex.glsl", "/shaders/mainFragment.glsl");
        renderer = new Renderer(window, shader);
        window.setBackground(0, 0, 0);
        window.create();
        mesh.create();
        meshRoad.create();
        meshCube.create();
        fuelMesh.create();
        shader.create();

        //THIRD PERSON CAMERA
        //objects.add(cube);
        for (int i = 0; i < 100; i++) {
            if(i%2 == 0) {
                objects.add(new GameObject(new Vector3f(2 +  i, 1, 10+10* i),
                        new Vector3f(0, 0, 0), new Vector3f(1, 1, 1), meshCube, GameObject.Type.CUBE));

                objects.add(new GameObject(new Vector3f(  i, 1,  i),
                        new Vector3f(0, 0, 0), new Vector3f(1, 1, 1), fuelMesh, GameObject.Type.FUEL));
            }
            else {
                objects.add(new GameObject(new Vector3f(2 - i, 1, 10+10* i),
                        new Vector3f(0, 0, 0), new Vector3f(1, 1, 1), meshCube, GameObject.Type.CUBE));
            }
        }


        Shader fontshader = new Shader("/shaders/fontVertex.glsl","/shaders/fontFragment.glsl");
        fontshader.create();

        textManager = new TextManager();
        //font = new FontType(FileUtils.loadAsString("/font/calibri.png"),new File(getClass().getResource("calibri.fnt").getPath()));

//        font = new FontType(getClass().getResource("/calibri.png").getPath(),
//                new File(getClass().getResource("/calibri.fnt").getPath()));

        font = new FontType("C:\\Users\\HP\\Desktop\\lwjgl3-tutorial\\LWJGL Project\\resources\\calibri.png", new File("C:\\Users\\HP\\Desktop\\lwjgl3-tutorial\\LWJGL Project\\resources\\calibri.fnt"));

        timeText = new GUIText("Time: 0", 2, font, new Vector2f(0,0),1f,false);
        timeText.setColour(1,1,1);

        healthText = new GUIText("HP: " + healthPoint, 2,font, new Vector2f(0,0.1f),1f,false);
        healthText.setColour(1,1,1);

        fuelText = new GUIText ("Fuel: " + fuel, 2, font, new Vector2f(0,0.9f),1f,false);
        fuelText.setColour(0,0,0);



        TextManager.loadText(healthText);
        TextManager.loadText(timeText);


    }

    @Override
    public void run() {
        init();
        while(!window.shouldClose() && !Input.isKeyDown(GLFW.GLFW_KEY_ESCAPE)){ //Exit when esc key is pressed

            Long start = Calendar.getInstance().getTimeInMillis();

            update();
            DoCollisions();
            render();
            if(Input.isKeyDown(GLFW.GLFW_KEY_F11)) window.setFullscreen(!window.isFullscreen()); //sets the opposite of what the current screen is
            if(Input.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_LEFT))window.mouseState(true); //locks or unlocks the mouse (FIRST PERSON CAMERA)

            Long end = Calendar.getInstance().getTimeInMillis();

            if(end-start < 1000/FPS) {
                try {
                    Thread.sleep(1000/FPS - (end-start));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        close();
    }

    private void update() {
        window.update();
        camera.update(spaceship);
        if(healthPoint > 0 && fuel > 0) {
            spaceship.update();
            updateTime();
        }
        updateHealth();
        if(fuel == 0){
            gameOverText = new GUIText("GAME OVER", 10, font, new Vector2f(0, 0.1f), 1f, true);
            gameOverText.setColour(255, 0, 0);
        }

        //DoCollisions();
    }

    private void updateTime() {
        TextManager.removeText(timeText);
        long time = (Calendar.getInstance().getTimeInMillis() - startTime) / 1000;
        timeText.setTextString("Time: " + time);
        TextManager.loadText(timeText);
        if(time% 3 == 0 && time != lastFuelTime) {
            decreaseFuel();
            lastFuelTime = time;
        }
    }

    private void updateHealth() {
        TextManager.removeText(healthText);
        healthText.setTextString("HP: " + (healthPoint));
        TextManager.loadText(healthText);

    }

    private void increaseFuel() {
        TextManager.removeText(fuelText);
        fuel = fuel + 10;
        fuelText.setTextString("Fuel: " + fuel);
        TextManager.loadText(fuelText);
    }

    private void decreaseFuel(){
        TextManager.removeText(fuelText);
        fuel = fuel - 10;
        fuelText.setTextString("Fuel: " + fuel);
        TextManager.loadText(fuelText);

    }

    private void render() {
        //THIRD PERSON CAMERA
        for (GameObject object : objects) {
            renderer.renderObject(object, camera,light);
        }
        renderer.renderObject(road,camera,light);
        renderer.renderObject(spaceship, camera, light);

        TextManager.render();
        window.swapBuffers();
    }

    public void close(){ //Where our destroy methods is
        window.destroy();
        mesh.destroy();
        shader.destroy();
    }

//    //AABB - AABB Collision
//    public boolean checkCollision(GameObject one, GameObject two){
//        //Check for collision on x-axis
//        boolean collisionX = one.getPosition().getX() + one.maxX >= two.getPosition().getX() && two.getPosition().getX() + two.minX >= one.getPosition().getX();
//
//        //Check for collision on y-axis
//        boolean collisionY = one.getPosition().getY() + one.maxY >= two.getPosition().getY() && two.getPosition().getY() + two.minY >= one.getPosition().getY();
//
//
//        boolean collisionZ = one.getPosition().getZ() + one.maxZ >= two.getPosition().getZ() && two.getPosition().getZ() + two.minZ >= one.getPosition().getZ();
//
//        //collision occured only if both axes overlap
//        return collisionX && collisionY && collisionZ;
//    }

    public static boolean checkCollision(GameObject object1, GameObject object2) {
//        if (!object1.isRendered() || !object2.isRendered())
//            return false;

        Vector3f maxObj1 = new Vector3f(
                object1.getMaxX() + object1.getPosition().getX(),
                object1.getMaxY() + object1.getPosition().getY(),
                object1.getMaxZ() + object1.getPosition().getZ()
        );

        Vector3f maxObj2 = new Vector3f(
                object2.getMaxX() + object2.getPosition().getX(),
                object2.getMaxY() + object2.getPosition().getY(),
                object2.getMaxZ() + object2.getPosition().getZ()
        );

        Vector3f minObj1 = new Vector3f(
                object1.getMinX() + object1.getPosition().getX(),
                object1.getMinY() + object1.getPosition().getY(),
                object1.getMinZ() + object1.getPosition().getZ()
        );

        Vector3f minObj2 = new Vector3f(
                object2.getMinX() + object2.getPosition().getX(),
                object2.getMinY() + object2.getPosition().getY(),
                object2.getMinZ() + object2.getPosition().getZ()
        );

        return (
                        (minObj1.getX() <= maxObj2.getX() && maxObj1.getX() >= minObj2.getX()) &&
                        (minObj1.getY() <= maxObj2.getY() && maxObj1.getY() >= minObj2.getY()) &&
                        (minObj1.getZ() <= maxObj2.getZ() && maxObj1.getZ() >= minObj2.getZ())
        );
    }

    public void DoCollisions(){

        List<GameObject> toBeDeleted = new ArrayList<>();

        for(GameObject object : objects){
            if(object != spaceship && object.isRendered()){
                if(checkCollision(spaceship, object)){
                    System.out.println("Collision detected !");
                    toBeDeleted.add(object);
                    if(object.type== GameObject.Type.CUBE){
                        if(healthPoint>10){
                            healthPoint = healthPoint-10;
                      }
                        else {
                             healthPoint = 0;
                             updateHealth();
                             gameOverText = new GUIText("GAME OVER", 10, font, new Vector2f(0, 0.1f), 1f, true);
                             gameOverText.setColour(255, 0, 0);
                        }

                    }

                    else if(object.type == GameObject.Type.FUEL){
                        increaseFuel();

                    }

                }
            }
        }

        objects.removeAll(toBeDeleted);


    }

    public static void main(String[] args) {
        new Main().start();
    }


}
