package engine.hud;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Represents a font. It holds the font's texture atlas as well as having the
 * ability to create the quad vertices for any text using this font.
 *
 * @author Karl
 *
 */
public class FontType {

    private int textureAtlas;
    private TextMeshCreator loader;

    public FontType(String textureAtlasPath, File fontFile) {
        try {
            this.textureAtlas = TextureLoader.getTexture("PNG",new FileInputStream(new File(textureAtlasPath)))
                    .getTextureID();
        } catch (IOException e) {
            e.printStackTrace();
        }
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER,
                GL11.GL_LINEAR_MIPMAP_LINEAR);
        GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, 0f);
        this.loader = new TextMeshCreator(fontFile);
    }


    public int getTextureAtlas() {
        return textureAtlas;
    }


    public TextMeshData loadText(GUIText text) {
        return loader.createTextMesh(text);
    }

}

