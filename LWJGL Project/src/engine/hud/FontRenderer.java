package engine.hud;

import java.util.List;
import java.util.Map;

import engine.graphics.Shader;
import engine.utils.FileUtils;
import engine.hud.GUIText;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;



public class FontRenderer {

    private Shader shader;

    public FontRenderer(){
        try{

            String vertexFile = "/shaders/fontVertex.glsl";
                    //"C:\\Users\\HP\\Desktop\\lwjgl3-tutorial\\LWJGL Project\\resources\\shaders\\fontVertex.glsl";
                    //this.getClass().getResource("/shaders/fontVertex.glsl").getPath());
            String fragmentFile = "/shaders/fontFragment.glsl";
                    //"C:\\Users\\HP\\Desktop\\lwjgl3-tutorial\\LWJGL Project\\resources\\shaders\\fontFragment.glsl";
                    //this.getClass().getResource("/shaders/fontFragment.glsl").getPath());

            shader = new Shader(vertexFile,fragmentFile);
            shader.create();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public void render(Map<FontType, List<GUIText>> texts){

        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL30.glEnable(GL30.GL_BLEND);
        GL30.glBlendFunc(GL30.GL_SRC_ALPHA, GL30.GL_ONE_MINUS_SRC_ALPHA);
        shader.bind();
        for(FontType font : texts.keySet()){
            GL13.glActiveTexture(GL13.GL_TEXTURE0);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, font.getTextureAtlas());
            for(GUIText text : texts.get(font)){
                renderText(text);
            }
            GL13.glActiveTexture(0);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        }
        shader.unbind();
        GL11.glEnable(GL11.GL_DEPTH_TEST);

    }


    private void renderText(GUIText text){
        GL30.glBindVertexArray(text.getTextMeshVao());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        shader.setUniform("colour",text.colour);
        shader.setUniform("translation",text.getPosition());
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, text.getVertexCount());
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL30.glBindVertexArray(0);
    }


}
