package engine.graphics;

import engine.io.Window;
import engine.maths.Matrix4f;
import engine.objects.Camera;
import engine.objects.GameObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;

public class Renderer {
    private Shader shader;
    private Window window;

    public Renderer(Window window, Shader shader){
        this.shader = shader;
        this.window = window;
    }

    public void renderMesh(GameObject object, Camera camera){
        GL30.glBindVertexArray(object.getMesh().getVAO()); //enabling the vertex array

        GL30.glEnableVertexAttribArray(0); //enabling the first location
        GL30.glEnableVertexAttribArray(1); //enabling the second location (we added color)
        GL30.glEnableVertexAttribArray(2); //enabling the third location for textures

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, object.getMesh().getIBO()); // Enabling the index array
        GL13.glActiveTexture(GL13.GL_TEXTURE0); //Bind the texture, putting our texture in the 1st position in the texture buffer
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, object.getMesh().getMaterial().getTextureID());
        shader.bind();

        shader.setUniform("model", Matrix4f.transform(object.getPosition(), object.getRotation(), object.getScale())); //OUR OBJECT
        shader.setUniform("view", Matrix4f.view(camera.getPosition(), camera.getRotation())); //CAMERA
        shader.setUniform("projection", window.getProjectionMatrix());

        GL11.glDrawElements(GL11.GL_TRIANGLES, object.getMesh().getIndices().length, GL11.GL_UNSIGNED_INT, 0);
        //arg1: how are you gonna draw,
        //arg2: how many elements you wanna draw,
        //arg3: what type of numbers these are,
        //arg4: pointer (we dont have a pointer rn)

        shader.unbind(); //gets rid of the shader and gets ready for the next shader
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0); //disabling
        GL30.glDisableVertexAttribArray(0); //making sure nothing bad happens
        GL30.glDisableVertexAttribArray(1); //making sure nothing bad happens
        GL30.glDisableVertexAttribArray(2); //making sure nothing bad happens
        GL30.glBindVertexArray(0); //disable everything all together
    }

    public void renderSmt(Mesh mesh) {
        GL30.glBindVertexArray(mesh.getVAO());
        GL30.glEnableVertexAttribArray(0);
        GL30.glEnableVertexAttribArray(1);
        GL30.glEnableVertexAttribArray(2);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, mesh.getIBO());
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, mesh.getMaterial().getTextureID());
        shader.bind();
        GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getIndices().length, GL11.GL_UNSIGNED_INT, 0);
        shader.unbind();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL30.glDisableVertexAttribArray(0);
        GL30.glDisableVertexAttribArray(1);
        GL30.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
    }

     public void renderObject(GameObject object, Camera camera, Light light)
    {
        //Bind all
        GL30.glBindVertexArray(object.getMesh().getVAO());
        GL15.glBindBuffer(GL30.GL_ARRAY_BUFFER,object.getMesh().getPBO());
        GL30.glEnableVertexAttribArray(0);
        GL30.glEnableVertexAttribArray(1);
        GL30.glEnableVertexAttribArray(2);
        GL30.glEnableVertexAttribArray(3);
        GL15.glBindBuffer(GL30.GL_ELEMENT_ARRAY_BUFFER,object.getMesh().getIBO());
        GL13.glActiveTexture(GL13.GL_TEXTURE0); //Bind the texture, putting our texture in the 1st position in the texture buffer
        GL13.glBindTexture(GL11.GL_TEXTURE_2D, object.getMesh().getMaterial().getTextureID());
        shader.bind();

        shader.setUniform("model",Matrix4f.transform(object.getPosition(),
                object.getRotation(),object.getScale()));
        shader.setUniform("projection",window.getProjectionMatrix());
        shader.setUniform("view", camera.getView());
        shader.setUniform("lightPosition", light.getPosition());
        shader.setUniform("lightColor", light.getColor());
        GL11.glDrawElements(GL11.GL_TRIANGLES,object.getMesh().getIndices().length, GL11.GL_UNSIGNED_INT, 0);

        //Unbind all
        shader.unbind();
        GL15.glBindBuffer(GL30.GL_ELEMENT_ARRAY_BUFFER,0);
        GL15.glBindBuffer(GL30.GL_ARRAY_BUFFER,0);
        GL30.glDisableVertexAttribArray(0);
        GL30.glDisableVertexAttribArray(1);
        GL30.glDisableVertexAttribArray(2);
        GL30.glDisableVertexAttribArray(3);
        GL30.glBindVertexArray(0);
    }

}
