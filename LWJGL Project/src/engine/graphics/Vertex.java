package engine.graphics;

import engine.maths.Vector2f;
import engine.maths.Vector3f;

public class Vertex {
    private Vector3f position;
    private Vector3f normal; //for 3d model loading

    private Vector2f textureCoord;

    public Vertex(Vector3f position, Vector3f normal,Vector2f textureCoord) {
        this.position = position;
        this.normal = normal; ////for 3d model loading
        //this.color = color;
        this.textureCoord = textureCoord;
    }



    public Vector3f getPosition() {
        return position;
    }


    public Vector2f getTextureCoord() {
        return textureCoord;
    }

    public Vector3f getNormal() {return normal; }
}
