package engine.graphics;

import engine.maths.Matrix4f;
import engine.maths.Vector2f;
import engine.maths.Vector3f;
import engine.utils.FileUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;

public class Shader {
    private String vertexFile, fragmentFile;
    private int vertexID, fragmentID, programID; // these are pointers, (what a program is that all the shaders compiled together to send off to the graphics card to actually run the shaders)

    public Shader(String vertexPath, String fragmentPath) {
        vertexFile = FileUtils.loadAsString(vertexPath);
        fragmentFile = FileUtils.loadAsString(fragmentPath);
    }

    public void create(){
        programID = GL20.glCreateProgram();

        vertexID = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);

        GL20.glShaderSource(vertexID, vertexFile);
        GL20.glCompileShader(vertexID);

        //Error checking needs to be done since we cannot know if an error occured or not
        if (GL20.glGetShaderi(vertexID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) { //Checks if its actually compiled or not
            System.err.println("Vertex Shader: " + GL20.glGetShaderInfoLog(vertexID));
            return;
        }

        fragmentID = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);

        GL20.glShaderSource(fragmentID, fragmentFile);
        GL20.glCompileShader(fragmentID);

        if (GL20.glGetShaderi(fragmentID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.err.println("Fragment Shader: " + GL20.glGetShaderInfoLog(fragmentID));
            return;
        }

        //Attaching created shaders to our program
        GL20.glAttachShader(programID, vertexID);
        GL20.glAttachShader(programID, fragmentID);

        GL20.glLinkProgram(programID);
        if (GL20.glGetProgrami(programID, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
            System.err.println("Program Linking: " + GL20.glGetProgramInfoLog(programID));
            return;
        }

        GL20.glValidateProgram(programID);
        if (GL20.glGetProgrami(programID, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
            System.err.println("Program Validation: " + GL20.glGetProgramInfoLog(programID));
            return;
        }
        //After we are done creating the program we delete the shaders, since we have a program that does that for us
    }

    //The way to upload data dynamically is through uniforms in the shader
    public int getUniformLocation(String name) {
        return GL20.glGetUniformLocation(programID, name); //gets the index of where the uniform is
    }

    public void setUniform(String name, float value) {
        GL20.glUniform1f(getUniformLocation(name), value);
    }

    public void setUniform(String name, int value) {
        GL20.glUniform1i(getUniformLocation(name), value);
    }

    public void setUniform(String name, boolean value) {
        GL20.glUniform1i(getUniformLocation(name), value ? 1 : 0);
    }

   // public void setUniform(String name, Vector2f value) {
     //   GL20.glUniform2f(getUniformLocation(name), value.getX(), value.getY());
   // }

    //public void setUniform(String name, Vector3f value) {
      //  GL20.glUniform3f(getUniformLocation(name), value.getX(), value.getY(), value.getZ());
    //}

    public void setUniform(String name, Matrix4f value) {
        //We need to transform the matrix to row major order to column major order because OpenGL reads it that way
        FloatBuffer matrix = MemoryUtil.memAllocFloat(Matrix4f.SIZE * Matrix4f.SIZE);
        matrix.put(value.getAll()).flip();
        GL20.glUniformMatrix4fv(getUniformLocation(name), true, matrix); //transpose = if its in column major order -> false, if its in row major order -> true
    }

    public void setUniform(String name, Vector3f vector)
    {
        FloatBuffer vectorBuffer = MemoryUtil.memAllocFloat(3);
        float[] values = new float[]{vector.getX(),vector.getY(),vector.getZ()};
        vectorBuffer.put(values).flip();
        GL20.glUniform3fv(getUniformLocation(name),vectorBuffer);
    }

    public void setUniform(String name, Vector2f vector)
    {
        FloatBuffer vectorBuffer = MemoryUtil.memAllocFloat(2);
        float[] values = new float[]{vector.getX(),vector.getY()};
        vectorBuffer.put(values).flip();
        GL20.glUniform2fv(getUniformLocation(name),vectorBuffer);
    }





    public void bind() {
        GL20.glUseProgram(programID);
    }

    public void unbind() {
        GL20.glUseProgram(0);
    }

    public void destroy() {
        GL20.glDetachShader(programID, vertexID);
        GL20.glDetachShader(programID, fragmentID);
        GL20.glDeleteShader(vertexID);
        GL20.glDeleteShader(fragmentID);
        GL20.glDeleteProgram(programID);
    }
}
