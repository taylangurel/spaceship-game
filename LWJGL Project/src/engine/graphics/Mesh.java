package engine.graphics;

//A Vertex Array Object (or VAO) is an object that describes how the vertex attributes are stored in a Vertex Buffer Object (or VBO).
//This means that the VAO is not the actual object storing the vertex data, but the descriptor of the vertex data

import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.system.MemoryUtil;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class Mesh {
    private Vertex[] vertices;
    private int[] indices;
    private Material material;
    private int vao, pbo, ibo, cbo, tbo, nbo;
    //Vertex array object, Vertex buffer object(pbo): where our vertices are gonna be stored, Indices buffer object: where our indices are gonna be stored, color buffer object, texture buffer object

    public Mesh(Vertex[] vertices, int[] indices, Material material) {
        this.vertices = vertices;
        this.indices = indices;
        this.material = material;
    }

    public void create(){
        material.create();

        vao = GL30.glGenVertexArrays(); //Creating a new vertex array object
        GL30.glBindVertexArray(vao); //Binding the vao to the current one, whatever buffer we add onto it its gonna be added to this VAO

        ////POSITION BUFFER///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        FloatBuffer positionBuffer = MemoryUtil.memAllocFloat(vertices.length * 3); // in order to make the pbo readable by OpenGL
        float[] positionData = new float[vertices.length * 3];
        for(int i = 0; i < vertices.length; i++){
            positionData[i*3] = vertices[i].getPosition().getX();
            positionData[i*3 + 1] = vertices[i].getPosition().getY();
            positionData[i*3 + 2] = vertices[i].getPosition().getZ();

            //What this does is that source all X Y Z s into one array so that we can actually store it in a buffer to send it off to our GPU
        }
        positionBuffer.put(positionData).flip(); //flip() is for OpenGL

        pbo = storeData(positionBuffer, 0, 3);

        ////COLOR BUFFER////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        FloatBuffer colorBuffer = MemoryUtil.memAllocFloat(vertices.length * 3); // in order to make this readable by OpenGL
//        float[] colorData = new float[vertices.length * 3];
//        for(int i = 0; i < vertices.length; i++){
//            colorData[i*3] = vertices[i].getColor().getX();
//            colorData[i*3 + 1] = vertices[i].getColor().getY();
//            colorData[i*3 + 2] = vertices[i].getColor().getZ();
//        }
//        colorBuffer.put(colorData).flip(); //flip() is for OpenGL
//
//        cbo = storeData(colorBuffer, 1, 3); // store in the 2nd position and it has 3 elements (r, g, b)

        ////TEXTURE BUFFER////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        FloatBuffer textureBuffer = MemoryUtil.memAllocFloat(vertices.length * 2); // in order to make this readable by OpenGL
        float[] textureData = new float[vertices.length * 2];
        for(int i = 0; i < vertices.length; i++){
            textureData[i*2] = vertices[i].getTextureCoord().getX();
            textureData[i*2 + 1] = vertices[i].getTextureCoord().getY();
        }
        textureBuffer.put(textureData).flip(); //flip() is for OpenGL

        tbo = storeData(textureBuffer, 2, 2); // store in the 3rd position and it has 2 elements (x,y)

        ////INDICES BUFFER//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        IntBuffer indicesBuffer = MemoryUtil.memAllocInt(indices.length);
        indicesBuffer.put(indices).flip();

        ibo = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, ibo); //Elements buffer is for the order of the drawing, normal array buffer just means how to draw it
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);


        nbo = GL15.glGenBuffers();

        ////Normal BUFFER///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        FloatBuffer normalBuffer = MemoryUtil.memAllocFloat(vertices.length * 3); // in order to make the pbo readable by OpenGL
        float[] normalData = new float[vertices.length * 3];
        for(int i = 0; i < vertices.length; i++){
            normalData[i*3] = vertices[i].getNormal().getX();
            normalData[i*3 + 1] = vertices[i].getNormal().getY();
           normalData[i*3 + 2] = vertices[i].getNormal().getZ();

            //What this does is that source all X Y Z s into one array so that we can actually store it in a buffer to send it off to our GPU
        }
        normalBuffer.put(normalData).flip(); //flip() is for OpenGL

        nbo = storeData(normalBuffer, 3, 3);

    }




    private int storeData(FloatBuffer buffer, int index, int size){
        int bufferID = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, bufferID); //Binding the vbo
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW); //putting the buffer data into the buffer object
        // (index -> where do wanna store, size -> how big each piece of data (x,y,z) in our case, type, normalized, stride, pointer) normalized, stride and pointer is for specific cases
        GL20.glVertexAttribPointer(index, size, GL11.GL_FLOAT, false, 0, 0); //Atribpointer allows shaders to get the data from vertex array object
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0); //Unbinding the buffer
        return bufferID;
    }

    //This gets rid of all the buffers
    public void destroy(){
        GL15.glDeleteBuffers(pbo);
        GL15.glDeleteBuffers(cbo);
        GL15.glDeleteBuffers(ibo);
        GL15.glDeleteBuffers(tbo);

        GL30.glDeleteVertexArrays(vao);

        material.destroy();
    }

    public Vertex[] getVertices() {
        return vertices;
    }

    public int[] getIndices() {
        return indices;
    }

    public int getVAO() {
        return vao;
    }

    public int getPBO() {
        return pbo;
    }

    public int getCBO(){
        return cbo;
    }

    public int getTBO(){
        return tbo;
    }

    public int getIBO() {
        return ibo;
    }

    public Material getMaterial(){
        return material;
    }
}
