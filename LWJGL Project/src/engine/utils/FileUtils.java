package engine.utils;

import java.io.*;
import java.util.Scanner;

//Helper functions class
public class FileUtils {



    public static String loadFileAsString(String filePath) throws FileNotFoundException{
        return loadFileAsString(new File(filePath));
    }

    public static String loadFileAsString(File file) throws FileNotFoundException {
        StringBuffer stringBuffer = new StringBuffer();

        Scanner scanner = new Scanner(new FileInputStream(file));

        while (scanner.hasNextLine())
        {
            stringBuffer.append(scanner.nextLine()).append(System.lineSeparator());
        }

        return stringBuffer.toString();
    }


 public static String loadAsString(String path) {
      StringBuilder result = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(FileUtils.class.getResourceAsStream(path)))) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                result.append(line).append("\n");
            }
        } catch (IOException e) {
            System.err.println("Couldn't find the file at " + path);
        }
        return result.toString();
    }
}
