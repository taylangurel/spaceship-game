package engine.io;

import org.lwjgl.glfw.*;

//We will use callbacks, what a callback is that everytime we do an event, it is listed and it goes to that callback
//So we will use callbacks so that we can use those events to control our window
public class Input {
    private static boolean[] keys = new boolean[GLFW.GLFW_KEY_LAST]; //Stores which keys are pressed in a boolean array, GLFW_KEY_LAST is for getting all the keys supported by GLFW
    private static boolean[] buttons = new boolean[GLFW.GLFW_MOUSE_BUTTON_LAST]; //Same as above
    private static double mouseX, mouseY;
    private static double scrollX, scrollY;

    private GLFWKeyCallback keyboard; //Gets the keyboard clicks
    private GLFWCursorPosCallback mouseMove; //Gets the x's and y's for our mouses position
    private GLFWMouseButtonCallback mouseButtons; //Gets the mouse clicks
    private GLFWScrollCallback mouseScroll; // Gets the scroll of the mouse

    public Input() {
        //Saves which keys are pressed
        keyboard = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) { //action = pressed/released || key = which key is pressed
                keys[key] = (action != GLFW.GLFW_RELEASE); // press/release check-> if the action is not release it means it is a press, thus it will return true
            }
        };

        //Saves the mouses X and Y position
        mouseMove = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double xpos, double ypos) { //action = pressed/released || key = which key is pressed
                mouseX = xpos;
                mouseY = ypos;
            }
        };

        //Saves the pressed mouse buttons
        mouseButtons = new GLFWMouseButtonCallback() {
            @Override
            public void invoke(long window, int button, int action, int mods) { //action = pressed/released || key = which key is pressed
                buttons[button] = (action != GLFW.GLFW_RELEASE); // press/release check-> if the action is not release it means it is a press, thus it will return true
            }
        };

        mouseScroll = new GLFWScrollCallback() {
            @Override
            public void invoke(long window, double offsetx, double offsety) {
                scrollX += offsetx;
                scrollY += offsety;
            }
        };
    }

    public static boolean isKeyDown(int key){
        return keys[key];
    }

    public static boolean isButtonDown(int button){
        return buttons[button];
    }

    //To destroy the callbacks after they are done creating
    public void destroyCallbacks(){
        keyboard.free();
        mouseMove.free();
        mouseButtons.free();
        mouseScroll.free();
    }

    public static double getMouseX() {
        return mouseX;
    }

    public static double getMouseY() {
        return mouseY;
    }

    public static double getScrollX() {
        return scrollX;
    }

    public static double getScrollY() {
        return scrollY;
    }

    public GLFWKeyCallback getKeyboardCallback() {
        return keyboard;
    }

    public GLFWCursorPosCallback getMouseMoveCallback() {
        return mouseMove;
    }

    public GLFWMouseButtonCallback getMouseButtonsCallback() {
        return mouseButtons;
    }

    public GLFWScrollCallback getMouseScrollCallback() {
        return mouseScroll;
    }
}
