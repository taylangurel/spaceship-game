package engine.io; //This package is for input and output components

import engine.maths.Matrix4f;
import engine.maths.Vector3f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;


public class Window {
    private int width, height;
    private String title;
    private long window;

    private int frames;
    private static long time;

    private Input input;

    private Vector3f background = new Vector3f(0,0,0); //called clear color in OpenGL terms

    private GLFWWindowSizeCallback sizeCallback; // Sets the new width and height of the window

    private boolean isResized;
    private boolean isFullscreen;
    private int[] windowPosx = new int[1], windowPosy = new int[1]; // variables that will store windows x and y position

    private Matrix4f projection;

    ////METHODS////

    public Window(int width, int height, String title) {
        this.width = width;
        this.height = height;
        this.title = title;
        projection = Matrix4f.projection(70.0f, (float) width / (float) height, 0.1f, 1000.0f);
    }

    public void create(){
        //Check if the GLFW initialization is done
        if(!GLFW.glfwInit()){
            System.err.println("ERROR: GLFW wasn't initialized.");
            return;
        }

        input = new Input();

        window = GLFW.glfwCreateWindow(width, height, title, isFullscreen ? GLFW.glfwGetPrimaryMonitor() : 0, 0);

        //Check if the window is created
        if(window == 0){
            System.err.println("ERROR: Window wasn't created.");
            return;
        }

        //Setting the window
        GLFWVidMode videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor()); //Where is it going to be displayed

        // Centers the window in our screen
        windowPosx[0] = (videoMode.width() - width) / 2;
        windowPosy[0] = (videoMode.height() - height) / 2;
        GLFW.glfwSetWindowPos(window, windowPosx[0], windowPosy[0]);
        GLFW.glfwMakeContextCurrent(window);
        GL.createCapabilities(); //adds the ability to render
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Since we will work 3d we have to enable depth

        createCallbacks();

        //Show the window
        GLFW.glfwShowWindow(window);

        // 1 -> The window is limited to screens refresh rate
        GLFW.glfwSwapInterval(1);

        time = System.currentTimeMillis();
    }

    private void createCallbacks(){
        sizeCallback = new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int w, int h) {
                //Everytime we resize the window, new width and height is saved
                width = w;
                height = h;
                isResized = true;
            }
        };

        //Setting the callbacks of keyboard, mouse movement and mouse buttons from the Input class
        GLFW.glfwSetKeyCallback(window, input.getKeyboardCallback());
        GLFW.glfwSetCursorPosCallback(window, input.getMouseMoveCallback());
        GLFW.glfwSetMouseButtonCallback(window, input.getMouseButtonsCallback());
        GLFW.glfwSetScrollCallback(window, input.getMouseScrollCallback());
        GLFW.glfwSetWindowSizeCallback(window, sizeCallback);
    }

    //This function gets all the callbacks connected to this window
    public void update(){
        if(isResized) { //If the window is resized change the viewport
            GL11.glViewport(0, 0, width, height);
            isResized = false;
        }
        GL11.glClearColor(background.getX(), background.getY(), background.getZ(), 1.0f); // gl11 is the version 1.1, and 1.0f -> f signifies its a float
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); //We have to clear the buffer which we are working on
        GLFW.glfwPollEvents();

        //To show frame rate
        frames++;
        if(System.currentTimeMillis() > time + 1000){
            GLFW.glfwSetWindowTitle(window, title + " | FPS: " + frames);
            time = System.currentTimeMillis();
            frames = 0;
        }
    }

    public void swapBuffers(){
        GLFW.glfwSwapBuffers(window);
    }

    //This function is needed in order to close the window when 'X' is clicked
    public boolean shouldClose(){
        return GLFW.glfwWindowShouldClose(window);
    }

    public void destroy(){
        input.destroyCallbacks();
        sizeCallback.free();
        GLFW.glfwWindowShouldClose(window);
        GLFW.glfwDestroyWindow(window);
        GLFW.glfwTerminate();
    }

    public void setBackground(float r, float g, float b){
        background.set(r, g, b);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getTitle() {
        return title;
    }

    public long getWindow() {
        return window;
    }

    public Matrix4f getProjectionMatrix() {
        return projection;
    }

    public boolean isFullscreen() {
        return isFullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        isFullscreen = fullscreen;
        isResized = true;
        if(isFullscreen){
            GLFW.glfwGetWindowPos(window, windowPosx, windowPosy);
            GLFW.glfwSetWindowMonitor(window, GLFW.glfwGetPrimaryMonitor(), 0, 0, width, height, 0);
        }
        else {
            GLFW.glfwSetWindowMonitor(window, 0, windowPosx[0], windowPosy[0], width, height, 0); // Setting the window position where the last position was before the fullscreen
        }
    }

    public void mouseState (boolean lock){ //FIRST PERSON CAMERA
        //Locks or unlocks the mouse, lock -> locks the mouse to the center of the window
        GLFW.glfwSetInputMode(window, GLFW.GLFW_CURSOR, lock ? GLFW.GLFW_CURSOR_DISABLED : GLFW.GLFW_CURSOR_NORMAL);
    }
}
