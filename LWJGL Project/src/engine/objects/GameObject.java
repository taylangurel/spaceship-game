package engine.objects;

import engine.graphics.Mesh;
import engine.graphics.Vertex;
import engine.io.Input;
import engine.maths.Vector3f;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GameObject {
    private Vector3f position, rotation, scale;
    private Mesh mesh;
    private  float moveSpeed = 0.2f; //Movement
    private boolean rendered = true;
    private float maxX,maxY,maxZ,minX,minY,minZ;
    public Type type = Type.CUBE;


    public GameObject(Vector3f position, Vector3f rotation, Vector3f scale, Mesh mesh, Type type) {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
        this.mesh = mesh;
        this.type = type;

        List<Vertex> vertices = Arrays.asList(mesh.getVertices());

        List<Float> xList = new ArrayList<>();
        List<Float> yList = new ArrayList<>();
        List<Float> zList = new ArrayList<>();

        for(Vertex vertex : vertices)
        {
            Vector3f vertexPosition = vertex.getPosition();
            xList.add(vertexPosition.getX());
            yList.add(vertexPosition.getY());
            zList.add(vertexPosition.getZ());
        }

        maxX = Collections.max(xList);
        maxY = Collections.max(yList);
        maxZ = Collections.max(zList);

        minX = Collections.min(xList);
        minY = Collections.min(yList);
        minZ = Collections.min(zList);
    }

    public void update(){
        //position.setZ(position.getZ() - 0.05f);

        //MOVEMENT
        if(Input.isKeyDown(GLFW.GLFW_KEY_D)) position = Vector3f.add(position, new Vector3f(-moveSpeed, 0, 0)); //LEFT
        if(Input.isKeyDown(GLFW.GLFW_KEY_A)) position = Vector3f.add(position, new Vector3f(moveSpeed, 0, 0)); //RIGHT
        if(Input.isKeyDown(GLFW.GLFW_KEY_S)) position = Vector3f.add(position, new Vector3f(0, 0, -moveSpeed)); //FORWARD
        if(Input.isKeyDown(GLFW.GLFW_KEY_W)) position = Vector3f.add(position, new Vector3f(0, 0, moveSpeed)); //BACKWARD
        if(Input.isKeyDown(GLFW.GLFW_KEY_SPACE)) position = Vector3f.add(position, new Vector3f(0, moveSpeed/2, 0)); //UP
        if(Input.isKeyDown(GLFW.GLFW_KEY_LEFT_SHIFT)) position = Vector3f.add(position, new Vector3f(0, -moveSpeed/2, 0));//DOWN

        //MOVEMENT
    }

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public Vector3f getScale() {
        return scale;
    }

    public Mesh getMesh() {
        return mesh;
    }

    public boolean isRendered(){
        return rendered;
    }

    public float getMaxX() {
        return maxX;
    }

    public float getMaxY() {
        return maxY;
    }

    public float getMaxZ() {
        return maxZ;
    }

    public float getMinX() {
        return minX;
    }

    public float getMinY() {
        return minY;
    }

    public float getMinZ() {
        return minZ;
    }

    public static enum Type{
        CUBE,SPACESHIP,FUEL,ROAD
    }
}
