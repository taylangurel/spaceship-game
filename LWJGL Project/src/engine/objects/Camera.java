package engine.objects;

import engine.io.Input;
import engine.maths.Matrix4f;
import engine.maths.Vector3f;
import org.lwjgl.glfw.GLFW;

public class Camera {
    private Vector3f position, rotation;
    private float moveSpeed = 0.05f;
    private double oldMouseX = 0, oldMouseY = 0, newMouseX, newMouseY; //FIRST PERSON CAMERA
    private float mouseSensitivity = 0.15f; //FIRST PERSON CAMERA
    private float distance = 9.0f, horizontalAngle = 0, verticalAngle = -20.0f; // THIRD PERSON CAMERA

    public Camera(Vector3f position, Vector3f rotation) {
        this.position = position;
        this.rotation = rotation;
    }

    public void update(){
        newMouseX = Input.getMouseX(); //FIRST PERSON CAMERA
        newMouseY = Input.getMouseY(); //FIRST PERSON CAMERA

        float x = (float) Math.sin(Math.toRadians(rotation.getY())) * moveSpeed; //FIRST PERSON CAMERA
        float z = (float) Math.cos(Math.toRadians(rotation.getY())) * moveSpeed; //FIRST PERSON CAMERA

        if(Input.isKeyDown(GLFW.GLFW_KEY_A)) position = Vector3f.add(position, new Vector3f(-z, 0, x));
        if(Input.isKeyDown(GLFW.GLFW_KEY_D)) position = Vector3f.add(position, new Vector3f(z, 0, -x));
        if(Input.isKeyDown(GLFW.GLFW_KEY_W)) position = Vector3f.add(position, new Vector3f(-x, 0, -z));
        if(Input.isKeyDown(GLFW.GLFW_KEY_S)) position = Vector3f.add(position, new Vector3f(x, 0, z));
        if(Input.isKeyDown(GLFW.GLFW_KEY_SPACE)) position = Vector3f.add(position, new Vector3f(0, moveSpeed, 0));
        if(Input.isKeyDown(GLFW.GLFW_KEY_LEFT_SHIFT)) position = Vector3f.add(position, new Vector3f(0, -moveSpeed, 0));

        //This gets the difference between mouse movement between frames
        float dx = (float) (newMouseX - oldMouseX); //FIRST PERSON CAMERA
        float dy = (float) (newMouseY - oldMouseY); //FIRST PERSON CAMERA

        rotation = Vector3f.add(rotation, new Vector3f(-dy * mouseSensitivity, -dx * mouseSensitivity, 0)); //FIRST PERSON CAMERA

        oldMouseX = newMouseX; //FIRST PERSON CAMERA
        oldMouseY = newMouseY; //FIRST PERSON CAMERA
    }

    public void update(GameObject object) {
        newMouseX = Input.getMouseX();
        newMouseY = Input.getMouseY();

        float dx = (float) (newMouseX - oldMouseX);
        float dy = (float) (newMouseY - oldMouseY);

        if (Input.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_LEFT)) {
            verticalAngle -= dy * mouseSensitivity;
            horizontalAngle += dx * mouseSensitivity;
        }
        if (Input.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_RIGHT)) {
            if (distance > 0) {
                distance += dy * mouseSensitivity / 4;
            } else {
                distance = 0.01f;
            }
        }

        float horizontalDistance = (float) (distance * Math.cos(Math.toRadians(verticalAngle)));
        float verticalDistance = (float) (distance * Math.sin(Math.toRadians(verticalAngle)));

        float xOffset = (float) (horizontalDistance * Math.sin(Math.toRadians(-horizontalAngle)));
        float zOffset = (float) (horizontalDistance * Math.cos(Math.toRadians(-horizontalAngle)));

        position.set(object.getPosition().getX() + xOffset, object.getPosition().getY() - verticalDistance +3, object.getPosition().getZ() - zOffset);

        rotation.set(verticalAngle, -horizontalAngle+180, 0);

        oldMouseX = newMouseX;
        oldMouseY = newMouseY;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public Matrix4f getView() {return Matrix4f.view(position,rotation);

    }
}
