#version 330

in vec2 pass_textureCoords;
in vec3 pass_colour;

out vec4 out_colour;


uniform sampler2D fontAtlas;

void main(void){

    out_colour = vec4(pass_colour, texture(fontAtlas, pass_textureCoords).a);

}
